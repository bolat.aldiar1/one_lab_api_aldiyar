import akka.actor.{ ActorSystem, Props }
import akka.http.scaladsl.Http
import routing.RestRoutes
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import services.ApiActor
import domain.User

object Boot extends App with RestRoutes {

  implicit val system = ActorSystem("api")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val users = scala.collection.mutable.Set[User]()
  val apiActor = Props(new ApiActor(users))

  lazy val routes: Route = routesApi
  Http().bindAndHandle(routes, "0.0.0.0", 8080)

}
