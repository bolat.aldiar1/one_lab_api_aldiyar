package domain


trait DomainObject extends Serializable

trait DomainEntity extends DomainObject

case class User(name: String, id: Long) extends DomainEntity
