package domain

case class CreateUser(user: User) extends DomainEntity

case class UpdateUser(user: User) extends DomainEntity

case class GetUser(id: Long) extends DomainEntity

case class DeleteUser(id: Long) extends DomainEntity