package routing

import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props, ReceiveTimeout }
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.server.{ RequestContext, RouteResult }
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import domain.{ DomainEntity, DomainObject }
import routing.PerRequest.{ WithActorRef, WithProps }
import serializers.Serializers

import scala.concurrent.Promise
import scala.concurrent.duration._


trait PerRequest extends Actor with ActorLogging with Json4sSupport with Serializers {

  import context._

  def r: RequestContext
  def target: ActorRef
  def message: DomainObject
  def p: Promise[RouteResult]
  def stan = System.nanoTime()

  setReceiveTimeout(60.seconds)

  target ! message

  override def receive: Receive = {
    case res: String => complete(StatusCode.int2StatusCode(202), res)
    case res: DomainEntity => complete(StatusCode.int2StatusCode(200), res)
    case ReceiveTimeout => complete(StatusCode.int2StatusCode(500), "Recived Timeout")
    case e: Exception => complete(StatusCode.int2StatusCode(500), "Recived Exception")
  }

  def complete(status: StatusCode, obj: => ToResponseMarshallable) = {

    val f = status match {
      case _ =>
        r.complete(obj)
    }

    f.onComplete(p.complete(_))

    stop(self)
  }

}

object PerRequest {

  case class WithActorRef(r: RequestContext, target: ActorRef, message: DomainObject, p: Promise[RouteResult]) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: DomainObject, p: Promise[RouteResult], actorName: Option[String] = None) extends PerRequest {
    lazy val target = actorName match {
      case Some(name) => context.actorOf(props, name)
      case _ => context.actorOf(props)
    }
  }
}

trait PerRequestCreator {

  def perRequest(r: RequestContext, target: ActorRef, message: DomainObject, p: Promise[RouteResult])(implicit system: ActorSystem) =
    system.actorOf(Props(new WithActorRef(r, target, message, p)))

  def perRequest(r: RequestContext, props: Props, message: DomainObject, p: Promise[RouteResult], actorName: Option[String] = None)(implicit system: ActorSystem) = actorName match {
    case Some(name) => system.actorOf(Props(new WithProps(r, props, message, p, actorName)), s"parent-$name")
    case _ => system.actorOf(Props(new WithProps(r, props, message, p, None)))
  }

}

