package routing

import akka.actor.{ ActorSystem, Props }
import akka.http.scaladsl.server.{ Route, RouteResult }
import akka.http.scaladsl.server.directives.BasicDirectives
import akka.stream.ActorMaterializer
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import domain.DomainObject
import scala.concurrent.Promise


trait BaseRoute extends Json4sSupport with BasicDirectives with PerRequestCreator {

  implicit def system: ActorSystem
  implicit val materializer: ActorMaterializer



  def handleRequest(targetProps: Props, message: DomainObject): Route = ctx => {
    val p = Promise[RouteResult]
    perRequest(ctx, targetProps, message, p)(system)
    p.future
  }

}

