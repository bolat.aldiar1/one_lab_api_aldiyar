package routing

import akka.http.scaladsl.server.Directives._
import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.PathDirectives.path
import domain._
import serializers.Serializers

trait RestRoutes extends BaseRoute with Serializers {

  def apiActor: Props

  lazy val routesApi: Route = {

    (path("user" / LongNumber) & get ){ id =>
      handleRequest(apiActor, GetUser(id))
    } ~
      (path("create") & put ) {
        entity(as[User]){ u =>
          handleRequest(apiActor, CreateUser(u))
        }
      } ~
       (path("update") & put){
         entity(as[User]) { u =>
           handleRequest(apiActor, UpdateUser(u))
         }
       } ~
       (path("delete"/IntNumber) & delete){ id =>
         handleRequest(apiActor, DeleteUser(id))
       }
  }


  }
