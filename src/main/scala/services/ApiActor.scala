package services

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.server.RequestContext
import domain.{CreateUser, DeleteUser, GetUser, UpdateUser}
import domain.User
import scala.concurrent.duration._

import akka.actor.Actor
import akka.actor.Timers
import akka.pattern.gracefulStop
import scala.concurrent.Await
import akka.actor.Stash


case class Request(rc: RequestContext, msg: String)

object ApiActor {
  def props(users: scala.collection.mutable.Set[User]): Props = Props(new ApiActor(users))
}

class ApiActor(users: scala.collection.mutable.Set[User]) extends Actor with ActorLogging with Stash {


  override def receive: Receive = {

    case msg @ GetUser(id) =>

      val res: Option[User] = users.find(u => u.id == id)
      res match {
        case Some(p) => sender ! p.name.toString
        case None => sender ! "there is no such user"
      }


    case msg @ CreateUser(user: User) =>
          users.add(user)
          sender ! "User successfully created"


    case msg @ DeleteUser(id) =>
      val delUser: Option[User] = users.find(u => u.id == id)
      delUser match {
        case Some(p) => {
          users.filter(u => u != p)
          sender ! "User successfully deleted"
        }
        case None => sender ! "Failed to delite user"
      }


    case msg @ UpdateUser(user: User) =>
      val delUser = users.filter(u => u.id != user.id)
      users.add(user)
      sender ! "User successfully updated"
  }

}
