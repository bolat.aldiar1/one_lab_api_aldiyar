package serializers

import domain.{CreateUser, User, DeleteUser, GetUser, UpdateUser}
import org.json4s
import org.json4s.{ShortTypeHints, jackson}
import org.json4s.native.Serialization


trait Serializers {

  implicit val formats = Serialization.formats(

    ShortTypeHints(List(classOf[User], classOf[CreateUser], classOf[UpdateUser], classOf[GetUser], classOf[DeleteUser]))

  )

  implicit val serialization = jackson.Serialization

}
