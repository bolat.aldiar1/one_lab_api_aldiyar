name := "it_lab_api"

version := "0.1"

scalaVersion := "2.12.8"

val akkaHttpVersion =  "10.1.1"
val akkaVersion = "2.5.9"
val json4sVersion = "3.5.4"

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-xml" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "org.json4s" %% "json4s-core" % json4sVersion,
  "org.json4s" %% "json4s-jackson" % json4sVersion,
  "org.json4s" %% "json4s-native" % json4sVersion,

  "de.heikoseeberger" %% "akka-http-json4s" % "1.20.1",
  "com.typesafe" % "config" % "1.3.3"
)
